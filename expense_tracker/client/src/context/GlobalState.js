import React, {createContext, useReducer} from 'react'
import AppReducer from './AppReducer'
import axios from 'axios'

//initial state
const initialState = {
    transactions : [],
    error : null
}

//global context
export const GlobalContext = createContext(initialState)

//provider component

export const GlobalProvider = ({ children }) => {
    const [state, dispatch] = useReducer(AppReducer, initialState)

    async function getTransactions(){

        try {
            const res = await axios.get('/api')

            dispatch({
                type : 'GET_TRANSACTIONS',
                payload: res.data.data
            })
        } catch (error) {
            dispatch({
                type : 'ERROR_TRANSACTIONS',
                payload: error.response.data.error
            })
        }

        
    }

    async function deleteTransaction(id){

        try {
            await axios.delete(`/api/${id}`);
      
            dispatch({
              type: 'DELETE_TRANSACTION',
              payload: id
            });
          } catch (error) {
            dispatch({
              type: 'TRANSACTION_ERROR',
              payload: error.response.data.error
            });
          }

        
    }

    async function addTransaction(transaction){
        const config = {
            headers :{
                'Content-type' : 'application/json'
            }
        }
        try {
            const res = await axios.post('/api', transaction, config)

            dispatch({
                type : 'ADD_TRANSACTION',
                payload: res.data.data
            })

        } catch (error) {

            dispatch({
              type: 'TRANSACTION_ERROR',
              payload: error.response.data.error
            });
        }

        
    }

    return(<GlobalContext.Provider value={{
        transactions : state.transactions,
        getTransactions,
        deleteTransaction,
        addTransaction,
        error : state.error
    }}>
        {children}
    </GlobalContext.Provider>)
}




/* 
           { id: 1, text: 'Flower', amount: -20 },
           { id: 2, text: 'Salary', amount: 300 },
           { id: 3, text: 'Book', amount: -10 },
           { id: 4, text: 'Camera', amount: 150 } */