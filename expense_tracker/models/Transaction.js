const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
    text : {
        type : String,
        
    },
    amount : {
        type : Number
    },
    
})

module.exports = mongoose.model('Transaction', TransactionSchema)

