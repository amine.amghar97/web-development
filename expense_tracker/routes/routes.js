
const express = require('express')
const router = express.Router()
//const bodyParser = require('body-parser')

const { getTransactions , addTransaction, deleteTransaction } = require('../controllers/transaction') 

//router.use(bodyParser.urlencoded({ extended: false }))

router
  .route('/')
  .get(getTransactions)
  .post(addTransaction);

router
  .route('/:id')
  .delete(deleteTransaction);

module.exports = router;