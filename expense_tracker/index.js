const express = require('express')
const path = require("path")
const dotenv = require('dotenv')
const connectDB = require('./config/db')
const bodyParser = require('body-parser')

dotenv.config({ path : './config/config.env'})

connectDB()

const app = express()

app.use(express.json());

const routes = require('./routes/routes')
app.use('/api',routes)

if(process.env.NODE_ENV == "production"){
    app.use(express.static('client/build'))
    
    app.get('*', (req,res) => res.sendFile(path.resolve(__dirname,"client","build","index.html")))
}


 app.use(bodyParser.json())
var urlencodedParser = bodyParser.urlencoded({ extended: false })
 

PORT = process.env.PORT || 6000


app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on Port ${PORT}`))