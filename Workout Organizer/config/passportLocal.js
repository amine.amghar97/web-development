const mongoose =  require("mongoose")
const User = require('./User')
LocalStrategy = require('passport-local').Strategy;

module.exports = (passport)=>{
    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'pass',
        passReqToCallback: true
    },

   function(req,email, password, done) {
    User.findOne({ email : email }, function(err, user, info) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, req.session.answer = 'Incorrect email.' );
      }
      if (!user.validPassword(password)) {
        return done(null, false, req.session.answer = 'Incorrect password.' );
      }
      console.log("Log in successfuuuuul")
      
      return done(null, user) 
    });
  }
))
}