

var GoogleStrategy = require( 'passport-google-oauth20' ).Strategy;
const mongoose =  require("mongoose")
const User = require('./User')

module.exports = (passport)=>{
    passport.use(new GoogleStrategy({
        clientID:     process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000/auth/google/callback",
      },
      async(request, accessToken, refreshToken, profile, done) => {
          
        const newUser = 
          { googleId: profile.id,
            username: profile.displayName,
            email: profile._json.email
            } 

          let user = await User.findOne({email : profile._json.email })
            try {
              if(user){
                done(null,user)
                
              }else{
                user  = await User.create(newUser)
                done(null,user)
              }
            } catch (error) {
                console.log(error)
            }


        //console.log(profile._json)
      }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
      });
      
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
}