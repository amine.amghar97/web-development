const mongoose  = require("mongoose")

var workoutSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    description : {
        type : String,
        required : true
    },
    image : {
        type : String,
        required : true
    },
    type : {
        type : String,
        default : 'All',
        enum : ['All','biceps','triceps','chest','abs','back','legs']
    }
})

var Workout  = mongoose.model('workout', workoutSchema)

module.exports = Workout