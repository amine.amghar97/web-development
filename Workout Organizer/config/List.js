const mongoose  = require("mongoose")
const Workout = require("./Workout")
const User = require("./User")

var listSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    description : {
        type : String
    },
    user : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'User'
    },
    workouts : {
        type : [],
        ref : 'Workout'
    }
        
})

var List  = mongoose.model('list', listSchema)

module.exports = List