module.exports = {
    ensureAuth : function(req,res,next){
        if(req.isAuthenticated()){
            next()
        }else{
            res.redirect("/login")
        }
    },

    ensureGuest : function(req,res,next){
        if(req.isAuthenticated()){
            res.redirect("/workouts")
        }else{
            next()
        }
    },

   
    
}