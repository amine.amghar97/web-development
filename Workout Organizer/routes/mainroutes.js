const express = require('express')
const router = express.Router()
const {ensureAuth, ensureGuest,authenticate} = require('../middleware/auth_mid_google')
const Workout = require('../config/Workout')
const List = require('../config/List')
const bodyParser = require('body-parser')
const User = require('../config/User')



var urlencodedParser = bodyParser.urlencoded({ extended: false })


router.get('/',ensureGuest,(req,res) => {
    res.render('mainp')
})

router.get('/login',(req,res) => {
    var message = req.session.answer
    req.session.answer = null

    res.render('login',{
        layout : 'login_layout',
        message
    })
    
})

router.get('/register',(req,res) => {
    res.render('register',{
        layout : 'login_layout'
    })
})

router.get('/logout',(req,res)=>{
    req.logout()
    res.redirect('/')
})

router.get('/contact',(req,res)=>{
    res.render('contact',{
        layout : 'main',
        authent : req.isAuthenticated()
    })
})
router.get('/about',(req,res)=>{
    res.render('about',{
        layout :'main',
        authent : req.isAuthenticated()
    })
})




router.get('/workouts', async (req,res) => {

    try {
        if(req.session.message == "Workout exist already" || req.session.message == "Please Choose a List after you choose your workout"){
           var msg_err = req.session.message
        }
        if(req.session.message == "Workout Added successfully"){
            var msg_success = req.session.message
         }
        req.session.message = null
        const workouts = await Workout.find().lean()
        if(req.isAuthenticated()){
            const lists = await List.find({user : req.user.id}).lean()
            res.render('workouts',{
                layout : 'main',
                workouts,
                authent : req.isAuthenticated(),
                lists,
                msg_success,
                msg_err
                
            })
        }else{
            
         res.render('workouts',{
            layout : 'main',
            workouts,
            authent : req.isAuthenticated()
        })
        }
         
    } catch (error) {
        console.log(error)
    }

    
})

router.post('/workouts', urlencodedParser, async (req,res) => {

    try {
        var workouts = {};
         if(req.body.search){
            workouts = await Workout.find({name : new RegExp (req.body.search)}).lean()
            
         }else if(req.body.musclename !== 'Choose Muscle'){
            workouts = await Workout.find({type : req.body.musclename}).lean()
            
         }else{
            workouts = await Workout.find().lean()
            
         }
        
         if(req.isAuthenticated()){
            const lists = await List.find({user : req.user.id}).lean()
            res.render('workouts',{
                layout : 'main',
                authent : req.isAuthenticated(),
                workouts,
                lists
            })
         }else{
            res.render('workouts',{
                layout : 'main',
                authent : req.isAuthenticated(),
                workouts
            })
         }



    } catch (error) {
        console.log(error)
    }

    
})

router.post('/workouts/add', ensureAuth, urlencodedParser, async (req,res) => {

     try {
        const list = await List.findOne({name : req.body.listname}).lean()
        
        
        
        
        var c = 0
        if(!req.body.listname){
            req.session.message = "Please Choose a List after you choose your workout"
            res.redirect('/workouts')
        }
        else{
            for(x = 0; x<list.workouts.length; x++){
                if(list.workouts[x] == req.body.chosenWorkout){
                    req.session.message = "Workout exist already"
                    res.redirect('/workouts')
                    c = 1
                }
            }
            if(c != 1){
                const update_list = await List.findOneAndUpdate({_id : list._id},{"$push" : {
                    "workouts" : req.body.chosenWorkout
                }},{
                    new : true,
                    runValidators : true
                } )
                
                req.session.message = "Workout Added successfully"
                
                
                //console.log("list id is : "+list._id+" list updated successfully, this is the list " + update_list.workouts)
                res.redirect('/workouts')
            }
        }
        
        


     } catch (error) {
         console.log(error)
     }

})

router.get('/mycollections',ensureAuth,async(req,res) => {

    try {
        if(req.session.message == "Please enter a valid name for your list" || req.session.message == "Please enter a valid description for your list"){
            var msg_err = req.session.message
        }
        if(req.session.message == "Workout List deleted" || req.session.message == "List Created"){
            var msg_success = req.session.message
        }
        
         req.session.message = null
        const lists = await List.find({user : req.user.id}).lean()

        res.render('collections',{
            layout : 'main',
            authent : req.isAuthenticated(),
            lists,
            msg_success,
            msg_err
        })
    } catch (error) {
        console.log(error)
    }
    
})

router.post('/mycollections', urlencodedParser,ensureAuth, async (req,res) => {

    try {
        req.body.user = req.user.id
        console.log(req.body.name + " and "+ req.body.description)
        if(req.body.name == ""){
            req.session.message = "Please enter a valid name for your list"
            res.redirect('/mycollections')
        }else if(!req.body.description){
            req.session.message = "Please enter a valid description for your list"
            res.redirect('/mycollections')
        }
        else{
            const list = await List.create(req.body)
            req.session.message = "List Created"
            res.redirect('/mycollections')
        }
        
        
        
    } catch (error) {
        console.log(error)
    }

    
})

router.post('/mycollections/delete', urlencodedParser,ensureAuth, async (req,res) => {

    try {
        
        await List.find({_id : req.body.list_id}).deleteOne()

        req.session.message = "Workout List deleted"
        res.redirect('/mycollections')
        
        
    } catch (error) {
        console.log(error)
    }

    
})

router.post('/mycollections/workouts/delete', urlencodedParser,ensureAuth, async (req,res) => {

    try {
        
        const list = await List.find({name : req.body.thislist})
        const workout = await Workout.find({name : req.body.remove_w})
       
        const update_list = await List.findOneAndUpdate({name : req.body.thislist},{"$pull" : {
            "workouts" : req.body.remove_w
        }},{
            new : true,
            runValidators : true
        } )

       
        req.session.message = "Workout removed"
        res.redirect('/mycollections/workouts/'+String(list[0]._id))

        
        
    } catch (error) {
        console.log(error)
    }

    
})

router.get('/mycollections/workouts/:id',ensureAuth,async(req,res) => {
    
    var exist = null
    w_names = [];
    workouts = [];
    try {
        
        if(req.session.message == "Workout removed"){
            var msg_success = req.session.message
        }
        req.session.message = null

        const lists = await List.find({_id : req.params.id}).lean()
        

        for(x=0; x<lists[0].workouts.length;x++){
            w_names.push(lists[0].workouts[x])
            
        }
        

        for(x=0; x<w_names.length;x++){
            const w = await Workout.find({name : w_names[x]}).lean()
            workouts.push(w[0])
        }
        
        if(workouts.length == 0){
           
            exist = "There are no workouts in this list. Try adding some workouts"
        }
        
       
        
        res.render('userworkouts',{
            layout : 'main',
            authent : req.isAuthenticated(),
            workouts,
            lists,
            msg_success,
            exist
        })
        
    } catch (error) {
        console.log(error)
    }
    
})







module.exports = router