const express = require('express')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const connectDB = require('./config/db')
const exphbs = require('express-handlebars')
const path = require('path')
const User = require('./config/User')
const bodyParser = require('body-parser')
const passport = require('passport')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const flash = require('connect-flash')
const methodOverride = require('method-override')

//dotenv
dotenv.config({path : './config/config.env'})

//passport require
require("./config/passport")(passport)
require("./config/passportLocal")(passport)

//db connection
connectDB()

const app = express()

//hbs
app.engine('.hbs', exphbs({defaultLayout : 'main', extname: '.hbs'}));
app.set('view engine', '.hbs');


//session

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store : new MongoStore({mongooseConnection : mongoose.connection})
    
  }))
  

//passport
app.use(passport.initialize())
app.use(passport.session())


//routes
app.use('/',require('./routes/mainroutes'))
app.use('/auth',require('./routes/auth'))

//static
app.use(express.static(path.join(__dirname,'static')))

//body parser
app.use(bodyParser.json())
var urlencodedParser = bodyParser.urlencoded({ extended: false })

//method override
app.use(methodOverride('_method'))

//connect-flash
app.use(flash())


// post register
app.post('/register',urlencodedParser, async(req,res) =>{

    var user = new User({
        username : req.body.username,
        email : req.body.email,
        
    })
    user.password = user.generateHash(req.body.pass)
    //check user existence
   try {
    const exist = await User.findOne({email : req.body.email}).lean()
    
    if(exist){
        console.log(exist)
        res.render('register',{
            layout : 'login_layout',
            msg : 'User already exists'
        })
    }else{
        user.save()
        console.log("New user registered")
        return res.redirect('/login')
    }
   } catch (error) {
       console.log(error)
   }
    

   
})


//post login passport
app.post('/login',urlencodedParser, 
    passport.authenticate('local',{ successRedirect: '/workouts',
                                    failureRedirect: '/login',
                                    failureFlash: true })
)





PORT = process.env.PORT || 3000
app.listen(PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`))